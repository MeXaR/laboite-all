# Tous les projets en une seule commande

- La boite: http://localhost:1000
- Sondage: http://localhost:2000
- Le blog API: http://localhost:3000
- Le blog Front: http://localhost:4000
- L'Agenda: http://localhost:5000

Pour le blog API, ne pas oublier de mettre `MONGO_URL="mongodb://127.0.0.1:27017/meteor"` dans le fichier .env
